package com.example.twitter.configuration;

import com.example.twitter.properties.TwitterProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;

/**
 * Spring configuration class for the Twitter api provider
 *
 */
@Configuration
@EnableConfigurationProperties(TwitterProperties.class)
public class TwitterConfiguration {

    @Bean
    public Twitter twitter(TwitterProperties properties) {
        if (properties.getAccessToken() == null || properties.getAccessTokenSecret() == null)
            throw new IllegalStateException("Twitter access token and access token secret should be provided");

        return new TwitterTemplate(properties.getConsumerKey(), properties.getConsumerSecret(),
                properties.getAccessToken(), properties.getAccessTokenSecret());
    }
}
