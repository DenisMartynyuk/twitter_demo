package com.example.twitter.model;


/**
 * Interface describing a twitter tweet
 *
 */
public interface TwitterTweet {
    /**
     * Get the unqiue tweets id
     *
     * @return the {@code long} id
     */
    long getId();

    /**
     * Get the tweets text
     *
     * @return the {@code String} text, never {@code null}
     */
    String getText();

    /**
     * Get id of the user that has posted this tweet
     *
     * @return the {@code long} userId
     */
    long getFromUserId();
}
