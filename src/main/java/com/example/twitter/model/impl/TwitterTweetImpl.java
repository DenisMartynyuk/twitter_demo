package com.example.twitter.model.impl;

import com.example.twitter.model.TwitterTweet;
import org.springframework.util.Assert;


public class TwitterTweetImpl implements TwitterTweet {
    private final long id;
    private final String text;
    private final long fromUserId;

    private TwitterTweetImpl(long id, String text, long fromUserId) {
        Assert.notNull(text, "tweet text should not be null");

        this.id = id;
        this.text = text;
        this.fromUserId = fromUserId;
    }

    public static TwitterTweet of(long id, String text, long fromUserId){
        return new TwitterTweetImpl(id, text, fromUserId);
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public long getFromUserId() {
        return fromUserId;
    }
}
