package com.example.twitter.controller;

import com.example.twitter.exception.TweetApiException;
import com.example.twitter.exception.TweetFollowNotFoundException;
import com.example.twitter.exception.TweetNotFoundException;
import com.example.twitter.exception.TweetServiceUnauthorizedException;
import com.example.twitter.model.TwitterTweet;
import com.example.twitter.service.TweetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Controller that handles requests for retweeting and following popular tweets
 *
 */
@RestController
@RequestMapping("tweets")
public class TwitterController {
    private final Logger log;
    private final TweetService service;

    @Autowired
    public TwitterController(TweetService service) {
        this.service = service;
        this.log = LoggerFactory.getLogger(TwitterController.class);
    }

    @GetMapping("/{query}")
    public HttpEntity<Void> retweetAndFollow(@PathVariable String query) {
        try {
            TwitterTweet tweet = service.getMostPopularTweetBy(query);

            service.followUserById(tweet.getFromUserId());
            service.retweet(tweet.getId());

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (TweetServiceUnauthorizedException ex1) {
            log.error("Can't find tweets because service unauthorized", ex1);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (TweetNotFoundException ex2) {
            log.warn("Can't find tweets for query:{}", query);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (TweetFollowNotFoundException ex3) {
            log.warn("Can't find most tweeted user to follow");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (TweetApiException ex4) {
            log.warn("Can't tweet due to", ex4);
            return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        } catch (Exception ex5) {
            log.error("Can't tweet due to", ex5);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
