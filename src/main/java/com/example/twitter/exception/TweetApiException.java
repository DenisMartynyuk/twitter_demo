package com.example.twitter.exception;


/**
 * Exception indicates underlie api provider error
 *
 */
public class TweetApiException extends RuntimeException {
    public TweetApiException(Throwable cause) {
        super(cause);
    }

    public TweetApiException(String message) {
        super(message);
    }
}
