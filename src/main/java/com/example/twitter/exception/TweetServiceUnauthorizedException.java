package com.example.twitter.exception;


/**
 * Exception indicates unauthorized access to underlie api provider
 *
 */
public class TweetServiceUnauthorizedException extends RuntimeException {
}
