package com.example.twitter.exception;


/**
 * Exception indicates no user was found by follow user request
 *
 */
public class TweetFollowNotFoundException extends RuntimeException {
}
