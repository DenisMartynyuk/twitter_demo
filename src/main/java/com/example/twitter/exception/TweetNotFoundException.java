package com.example.twitter.exception;


/**
 * Exception indicates no tweet was found by search request
 *
 */
public class TweetNotFoundException extends RuntimeException {
}
