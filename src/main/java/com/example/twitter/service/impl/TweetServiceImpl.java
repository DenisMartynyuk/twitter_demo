package com.example.twitter.service.impl;


import com.example.twitter.exception.TweetApiException;
import com.example.twitter.exception.TweetFollowNotFoundException;
import com.example.twitter.exception.TweetNotFoundException;
import com.example.twitter.exception.TweetServiceUnauthorizedException;
import com.example.twitter.model.TwitterTweet;
import com.example.twitter.model.impl.TwitterTweetImpl;
import com.example.twitter.service.TweetService;
import org.springframework.social.ApiException;
import org.springframework.social.MissingAuthorizationException;
import org.springframework.social.twitter.api.SearchParameters;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.NoSuchElementException;

@Service
public class TweetServiceImpl implements TweetService {
    private final Twitter twitter;

    public TweetServiceImpl(Twitter twitter) {
        this.twitter = twitter;
    }

    @Override
    public TwitterTweet getMostPopularTweetBy(String query) {
        try {
            Iterator<Tweet> resultIterator = twitter.searchOperations()
                    .search(mostPopularTweeParams(query))
                    .getTweets().iterator();

            Tweet topTweet = resultIterator.next();
            return TwitterTweetImpl.of(topTweet.getId(), topTweet.getText(), topTweet.getFromUserId());
        } catch (MissingAuthorizationException e3) {
            throw new TweetServiceUnauthorizedException();
        } catch (ApiException e1) {
            throw new TweetApiException(e1);
        } catch (NoSuchElementException e2) {
            throw new TweetNotFoundException();
        }
    }

    @Override
    public String followUserById(long userId) {
        try {
            String followedUser = twitter.friendOperations().follow(userId);
            if (followedUser == null) throw new TweetFollowNotFoundException();
            return followedUser;
        } catch (MissingAuthorizationException e3) {
            throw new TweetServiceUnauthorizedException();
        } catch (ApiException e1) {
            throw new TweetApiException(e1.getMessage());
        } catch (NoSuchElementException e2) {
            throw new TweetNotFoundException();
        }
    }

    @Override
    public TwitterTweet retweet(long tweetId) {
        try {
            Tweet tweet = twitter.timelineOperations().retweet(tweetId);
            if (tweet == null) throw new TweetNotFoundException();
            return TwitterTweetImpl.of(tweet.getId(), tweet.getText(), tweet.getFromUserId());
        } catch (MissingAuthorizationException e3) {
            throw new TweetServiceUnauthorizedException();
        } catch (ApiException e1) {
            throw new TweetApiException(e1.getMessage());
        }
    }

    SearchParameters mostPopularTweeParams(String query) {
        return new SearchParameters(query)
                .resultType(SearchParameters.ResultType.POPULAR)
                .count(1);
    }
}
