package com.example.twitter.service;

import com.example.twitter.exception.TweetApiException;
import com.example.twitter.exception.TweetFollowNotFoundException;
import com.example.twitter.exception.TweetNotFoundException;
import com.example.twitter.exception.TweetServiceUnauthorizedException;
import com.example.twitter.model.TwitterTweet;

/**
 * Service providing access to {@link TwitterTweet}s
 *
 */
public interface TweetService {

    /**
     * Find the most popular twitter tweet by specified query.
     *
     * @param query A UTF-8, URL-encoded search query of 500 characters maximum, including operators.
     *              Must not be {code null}
     * @return the most popular {@link TwitterTweet}. Must not be {code null}
     *
     * @throws TweetServiceUnauthorizedException if tweet service not authorized properly
     * @throws TweetNotFoundException if tweet not found for specified query
     * @throws TweetApiException if tweet not fetched due to internal api error
     */
    TwitterTweet getMostPopularTweetBy(String query);

    /**
     * Follow the user by specified id.
     *
     * @param userId user id to follow. Must not be {code null}
     * @return {@code String} username of the user that has been followed. Must not be {code null}
     *
     * @throws TweetServiceUnauthorizedException if tweet service not authorized properly
     * @throws TweetFollowNotFoundException if user not found for specified userid
     * @throws TweetApiException if tweet not fetched due to internal api error
     */
    String followUserById(long userId);

    /**
     * Retweet the tweet with specified id.
     *
     * @param tweetId The ID of the tweet to be retweeted. Must not be {code null}
     * @return the {@link TwitterTweet} object representing the retweet. Must not be {code null}
     *
     * @throws TweetServiceUnauthorizedException if tweet service not authorized properly
     * @throws TweetApiException if tweet not fetched due to internal api error
     * @throws TweetNotFoundException if tweet with specified id not found
     */
    TwitterTweet retweet(long tweetId);
}
