package com.example.twitter.service.impl;

import com.example.twitter.exception.TweetFollowNotFoundException;
import com.example.twitter.exception.TweetNotFoundException;
import com.example.twitter.model.TwitterTweet;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.springframework.social.twitter.api.*;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TweetServiceImplTest {
    private final String testQuery = "test_query";

    private TweetServiceImpl service;

    private SearchResults searchResultsMock;
    private Twitter twitterMock;

    @Before
    public void setUp() {
        this.twitterMock = mock(Twitter.class);
        this.service = new TweetServiceImpl(twitterMock);
        this.searchResultsMock = mock(SearchResults.class);

        SearchOperations searchOperationsMock = mock(SearchOperations.class);
        when(twitterMock.searchOperations()).thenReturn(searchOperationsMock);
        when(searchOperationsMock.search(any(SearchParameters.class))).thenReturn(searchResultsMock);

    }

    @Test
    public void testGetMostPopularTweetByWithProperResult() {
        long expectedId = 1;
        String expectedText = "expectedText";
        long expectedFromUserId = 123;

        Tweet expectedTweet = mock(Tweet.class);
        when(expectedTweet.getId()).thenReturn(expectedId);
        when(expectedTweet.getText()).thenReturn(expectedText);
        when(expectedTweet.getFromUserId()).thenReturn(expectedFromUserId);
        when(searchResultsMock.getTweets()).thenReturn(singletonList(expectedTweet));

        TwitterTweet tweet = service.getMostPopularTweetBy(testQuery);
        assertEquals(expectedId, tweet.getId());
        assertEquals(expectedText, tweet.getText());
        assertEquals(expectedFromUserId, tweet.getFromUserId());
    }

    @Test(expected = TweetNotFoundException.class)
    public void testGetMostPopularTweetByWithNotFoundResult() {
        when(searchResultsMock.getTweets()).thenReturn(Lists.emptyList());

        service.getMostPopularTweetBy(testQuery);
    }

    @Test
    public void testFollowUserByIdProperResult() {
        long userId = 1;
        String expectedUsername = "testUsername";
        FriendOperations friendOperationsMock = mock(FriendOperations.class);
        when(twitterMock.friendOperations()).thenReturn(friendOperationsMock);
        when(friendOperationsMock.follow(userId)).thenReturn(expectedUsername);

        String followedUsername = service.followUserById(userId);
        assertNotNull(followedUsername);
        assertEquals(expectedUsername, followedUsername);
    }

    @Test(expected = TweetFollowNotFoundException.class)
    public void testFollowUserByIdNotFound() {
        FriendOperations friendOperationsMock = mock(FriendOperations.class);
        when(twitterMock.friendOperations()).thenReturn(friendOperationsMock);

        service.followUserById(1);
    }

    @Test
    public void testRetweetProperResult() {
        long expectedTweetId = 1;
        String expectedText = "testText";
        long expectedUserId = 2;
        Tweet expectedTweet = mock(Tweet.class);
        when(expectedTweet.getId()).thenReturn(expectedTweetId);
        when(expectedTweet.getText()).thenReturn(expectedText);
        when(expectedTweet.getFromUserId()).thenReturn(expectedUserId);

        TimelineOperations timelineOperationsMock = mock(TimelineOperations.class);
        when(twitterMock.timelineOperations()).thenReturn(timelineOperationsMock);
        when(timelineOperationsMock.retweet(expectedTweetId)).thenReturn(expectedTweet);

        TwitterTweet retweededTweet = service.retweet(expectedTweetId);
        assertNotNull(retweededTweet);
        assertEquals(expectedTweetId, retweededTweet.getId());
        assertEquals(expectedText, retweededTweet.getText());
        assertEquals(expectedUserId, retweededTweet.getFromUserId());
    }

    @Test(expected = TweetNotFoundException.class)
    public void testRetweetNotFound() {
        TimelineOperations timelineOperationsMock = mock(TimelineOperations.class);
        when(twitterMock.timelineOperations()).thenReturn(timelineOperationsMock);

        service.retweet(1);
    }

    @Test
    public void testMostPopularTweeParams() {
        SearchParameters result = service.mostPopularTweeParams(testQuery);

        assertNotNull(result);
        assertEquals(testQuery, result.getQuery());
        assertEquals(SearchParameters.ResultType.POPULAR, result.getResultType());
        assertEquals(Integer.valueOf(1), result.getCount());
    }
}